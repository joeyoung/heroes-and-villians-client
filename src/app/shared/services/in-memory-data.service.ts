import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable()
export class InMemoryDataService implements InMemoryDataService {

  constructor() { }

  createDb() {
    const heroes = [
      { id: 11, name: 'Batman' },
      { id: 12, name: 'Superman' },
      { id: 13, name: 'Spiderman' },
      { id: 14, name: 'Thor' },
      { id: 15, name: 'Wolverine' },
      { id: 16, name: 'Wonder Woman' },
      { id: 17, name: 'Captain America' },
      { id: 18, name: 'Iron Man' },
      { id: 19, name: 'The Hulk' },
      { id: 20, name: 'Duke' }
    ];

    const villains = [
      { id: 11, name: 'Dr. Evil' },
      { id: 12, name: 'Lex Luthor' },
      { id: 13, name: 'Joker' },
      { id: 14, name: 'Cobra Commander' },
      { id: 15, name: 'Green Goblin' },
      { id: 16, name: 'Scarcrow' },
      { id: 17, name: 'Mr. Freeze' },
      { id: 18, name: 'Magneto' },
      { id: 19, name: 'Loki' },
      { id: 20, name: 'Kingpin' }
    ];
    return {heroes, villains};
  }
}
