import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { FormService } from './services/form.service';
import { FieldErrorDirective } from './directives/field-error.directive';

@NgModule({
  imports:      [ CommonModule ],
  declarations: [FieldErrorDirective],
  providers: [FormService],
  exports:      [ CommonModule, FormsModule, FieldErrorDirective ],
})
export class SharedModule { }
