import { Directive, ElementRef, OnInit, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appFieldError]'
})
export class FieldErrorDirective implements OnInit {

  private element: ElementRef;
  private renderer: Renderer2;

  constructor(el: ElementRef, renderer: Renderer2) {
    this.element = el;
    this.renderer = renderer;
  }

  ngOnInit() {
    this.renderer.addClass(this.element.nativeElement, 'custom-theme');
  }
}
