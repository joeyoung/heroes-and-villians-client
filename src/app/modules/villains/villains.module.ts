import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VillainsRoutesModule } from './villains.routes';
import { HomeComponent } from './views/home/home.component';

@NgModule({
  imports: [
    CommonModule,
    VillainsRoutesModule
  ],
  declarations: [HomeComponent]
})
export class VillainsModule { }
