import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { FormService } from '../../../../shared/services/form.service';

import { Hero } from '../../models/hero';

@Component({
  selector: 'hero-form',
  templateUrl: './hero-form.component.html',
  styleUrls: ['./hero-form.component.css']
})
export class HeroFormComponent implements OnInit, OnChanges {

  private formService: FormService;

  @Input()
  public hero: Hero;

  @Output()
  public onSave = new EventEmitter<Hero>();

  heroForm: FormGroup;

  name: FormControl;
  id: FormControl;

  constructor(formService: FormService) {
    this.formService = formService;
  }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
    this.bindFormToModel();
  }

  ngOnChanges() {
    if (this.heroForm) {
      this.bindFormToModel();
    }
  }

  createFormControls(): void {
    this.name = new FormControl('', [Validators.required, Validators.minLength(5)]);
    this.id = new FormControl();
  }

  createForm(): void {
    this.heroForm = new FormGroup({
      id: this.id,
      name: this.name
    });
  }

  bindFormToModel(): void {
    this.heroForm.reset({
      name: this.hero.name,
      id: this.hero.id
    });
  }

  submit(): void {
    // touch the control in case the user did not
    // this.touchAllFormFields(this.heroForm);
    this.formService.touchAllFields(this.heroForm);
    if (this.heroForm.dirty && !this.heroForm.valid) {
      return;
    }

    const formModel = this.heroForm.value;

    const hero: Hero = {
      id: this.hero.id || 0,
      name: formModel.name
    };

    this.onSave.emit(hero);
  }

  revert(): void {
    this.ngOnChanges();
  }
}
