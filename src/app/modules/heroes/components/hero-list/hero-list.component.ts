import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { HeroService } from '../../services/hero.service';
import { Hero } from '../../models/hero';

@Component({
  selector: 'hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.css']
})
export class HeroListComponent implements OnInit {

    private dataSubscription: Subscription;
    private loadingSubscription: Subscription;

    private heroService: HeroService;

    public heroes: Hero[] = [];
    public isLoading: boolean;

    @Output()
    public onHeroSelected = new EventEmitter<Hero>();

    constructor(heroService: HeroService) {
        this.heroService = heroService;
    }

    ngOnInit() {
        this.loadingSubscription = this.heroService.getLoadingStream()
            .subscribe(loading => {
                this.isLoading = loading;
            });

        this.dataSubscription = this.heroService.getDataStream()
            .subscribe(response => {
                this.heroes = response;
            });
    }

    ngOnDestory() {
        this.dataSubscription.unsubscribe();
        this.loadingSubscription.unsubscribe();
    }

    onSelect(hero: Hero) {
        this.onHeroSelected.emit(hero);
    }
}
