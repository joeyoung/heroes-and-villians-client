import { Component, OnInit } from '@angular/core';
import { HeroService } from '../../services/hero.service';

import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

@Component({
  selector: 'hero-search',
  templateUrl: './hero-search.component.html',
  styleUrls: ['./hero-search.component.css']
})
export class HeroSearchComponent implements OnInit {

  private heroService: HeroService;
  private searchSubject = new Subject<string>();

  constructor(heroService: HeroService) {
    this.heroService = heroService;
  }

    // Push a search term into the observable stream.
  search(term: string): void {
      this.searchSubject.next(term);
    }

  ngOnInit() {
    this.searchSubject
      .debounceTime(300)
      .distinctUntilChanged()
      .subscribe((term) => this.heroService.search(term));
  }
}
