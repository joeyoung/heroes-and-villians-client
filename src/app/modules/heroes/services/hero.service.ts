import { Injectable, EventEmitter } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import { Hero } from '../models/hero';

// https://blog.angular-university.io/how-to-build-angular2-apps-using-rxjs-observable-data-services-pitfalls-to-avoid/
// https://github.com/jhades/angular2-rxjs-observable-data-services/blob/master/src/state/TodoStore.ts

@Injectable()
export class HeroService {

    private heroesUrl = 'api/heroes';
    private headers = new HttpHeaders({'Content-Type': 'application/json'});

    private loadingSubject = new BehaviorSubject<boolean>(false);
    private dataSubject = new BehaviorSubject<Hero[]>([]);

    private _heroes = new Subject<Hero[]>();
    private _loading = new BehaviorSubject<boolean>(false);

    private http: HttpClient;

    constructor(http: HttpClient) {
        this.http = http;
    }

    public getLoadingStream(): Observable<boolean> {
        return this.loadingSubject.asObservable();
    }
    public getDataStream(): Observable<Hero[]> {
        return this.dataSubject.asObservable();
    }

    public get heroes$(): Observable<Hero[]> {
        return this._heroes.asObservable();
    }

    public get loading$(): Observable<boolean> {
        return this._loading.asObservable();
    }

    public load(): void {
        this.loadingSubject.next(true);
        this.http.get<Hero[]>(this.heroesUrl)
            .subscribe(data => {
                this.dataSubject.next(data);
                this.loadingSubject.next(false);
        });
    }

    public load2(): void {
        this._loading.next(true);
        this.http.get<Hero[]>(this.heroesUrl)
            .subscribe(
                (heroes) => this._heroes.next(heroes),
                (error) => {
                    this._loading.next(false);
                    console.log('Error retrieving Heroes');
                },
                () => this._loading.next(false)
            );
    }

    public search(term: string): void {
        this.loadingSubject.next(true);
        this.http.get<Hero[]>(`${this.heroesUrl}/?name=${term}`)
            .subscribe(data => {
                this.dataSubject.next(data);
                this.loadingSubject.next(false);
        });
    }

    public getById(id: number): Observable<Hero> {
        return this.http.get<Hero>(`${this.heroesUrl}/${id}`);
    }

    public update(hero: Hero): Observable<Hero> {
        return this.http
            .put<Hero>(`${this.heroesUrl}/${hero.id}`, JSON.stringify(hero), {headers: this.headers} );
    }

    public create(hero: Hero): Observable<Number> {
        return this.http.post<Number>(this.heroesUrl, hero);
    }
}
