import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

import { HeroesRoutesModule } from './heroes.routes';

import {HeroService } from './services/hero.service';
import { HomeComponent } from './views/home/home.component';
import { HeroListComponent } from './components/hero-list/hero-list.component';
import { HeroSearchComponent } from './components/hero-search/hero-search.component';
import { EditComponent } from './views/edit/edit.component';
import { NewComponent } from './views/new/new.component';
import { HeroesComponent } from './heroes.component';
import { HeroFormComponent } from './components/hero-form/hero-form.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    HeroesRoutesModule
  ],
  declarations: [HomeComponent, HeroListComponent, HeroSearchComponent, EditComponent, NewComponent, HeroesComponent, HeroFormComponent],
  providers: [HeroService]
})
export class HeroesModule { }
