import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeroService } from '../../services/hero.service';

import { Hero } from '../../models/hero';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    private router: Router;
    private heroService: HeroService;

    constructor(heroService: HeroService, router: Router) {
        this.heroService = heroService;
        this.router = router;
    }

    ngOnInit() {
        this.heroService.load();
    }

    heroSelected(hero: Hero) {
        this.router.navigate(['/heroes/edit', hero.id]);
    }
}
