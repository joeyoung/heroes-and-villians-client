import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { HeroService } from '../../services/hero.service';
import { Hero } from '../../models/hero';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {

    private heroService: HeroService;
    private router: Router;

    public hero: Hero = new Hero();

    public isOk: boolean;
    public alert: string;
    public saving: boolean;

    constructor(router: Router, heroService: HeroService) {
        this.router = router;
        this.heroService = heroService;
    }

    ngOnInit() {
    }

    saveHero(hero: Hero): void {
        this.saving = true;
        this.heroService.create(hero)
            .subscribe(response => {
                this.isOk = true;
                this.alert = 'Save Successful';
            }, error => {
                this.alert = 'Error Occured';
                this.isOk = false;
            }, () => {
                this.saving = false;
            });
    }
}
