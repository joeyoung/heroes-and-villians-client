import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { HeroService } from '../../services/hero.service';
import { Hero } from '../../models/hero';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

    private heroService: HeroService;
    private route: ActivatedRoute;

    public hero: Hero;
    public alert: string;
    public isOk: boolean;
    public saving: boolean;

  constructor(route: ActivatedRoute, heroService: HeroService) {
        this.heroService = heroService;
        this.route = route;
  }

    ngOnInit() {
        this.route.params.subscribe(params => {
            const id = params['id'];
            this.heroService.getById(id).subscribe(hero => {
                this.hero = hero;
            });
        });
    }

    saveHero(hero: Hero) {
        this.saving = true;
        this.heroService.update(hero)
            .subscribe(h => {
                this.isOk = true;
                this.alert = 'Update Successful';
            }, (error) => {
                this.isOk = false;
                this.alert = error;
            }, () => {
                this.saving = false;
            });
    }
}
