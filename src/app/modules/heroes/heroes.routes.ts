import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HeroesComponent } from './heroes.component';
import { HomeComponent } from './views/home/home.component';
import { EditComponent } from './views/edit/edit.component';
import { NewComponent } from './views/new/new.component';


const routes: Routes = [
  { path: 'heroes', component: HeroesComponent, children: [
      { path: '', component: HomeComponent },
      { path: 'new', component: NewComponent },
      { path: 'edit/:id', component: EditComponent }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class HeroesRoutesModule {}
