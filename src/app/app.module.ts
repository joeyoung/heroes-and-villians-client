import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { SharedModule} from './shared/shared.module';

import { AppRoutesModule } from './app.routes';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './shared/services/in-memory-data.service';

import { HeroesModule } from './modules/heroes/heroes.module';
import { VillainsModule } from './modules/villains/villains.module';

import { AppComponent } from './app.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found.component';

import { ApiUrlInterceptor } from './shared/services/api-url-interceptor';
import { environment } from '../environments/environment';
import {API_URL } from './app.injection-tokens';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    // HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, {dataEncapsulation: false}),
    SharedModule,
    HeroesModule,
    VillainsModule,
    AppRoutesModule
  ],
  providers: [
    { provide: API_URL, useValue: environment.apiUrl },
    { provide: HTTP_INTERCEPTORS, useClass: ApiUrlInterceptor, multi: true, deps: [API_URL] }
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
